# OpenML dataset: IRIS-flower-dataset

https://www.openml.org/d/43839

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Iris dataset was used in R.A. Fisher's classic 1936 paper, The Use of Multiple Measurements in Taxonomic Problems, and can also be found on the UCI Machine Learning Repository.
It includes three iris species with 50 samples each as well as some properties about each flower. One flower species is linearly separable from the other two, but the other two are not linearly separable from each other.
Content
The columns in this dataset are:
Id
SepalLengthCm
SepalWidthCm
PetalLengthCm
PetalWidthCm
Species

Acknowledgements
http://archive.ics.uci.edu/ml/index.php

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43839) of an [OpenML dataset](https://www.openml.org/d/43839). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43839/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43839/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43839/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

